from django.urls import path

from . import views

# The path() function can recieve four arguments
# We'll focus on the two arguments that are required, which are "route" and "view", and the third argument "name" which allows us to make global changes to the URL patterns of your project while only touching a single file
# Syntax
    # path(route, view, name)
urlpatterns = [
    path('', views.index, name='index'),
]